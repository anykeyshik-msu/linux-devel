#!/bin/bash

cd "${MESON_BUILD_ROOT}"

echo "Start testing..."

out=($(echo "Test123456" | ./$1 "[^0-9]{4}" "Passed" | cut -d ':' -f 3))
model_out=($(echo "Test123456" | sed -E "s/[^0-9]{4}/Passed/"))
if [ "$out" != "$model_out" ]; then
    echo "Non-digit test failed!"
    exit 1
fi

out=($(echo "Test123456" | ./$1 "[0-9]{5}" "Passed" | cut -d ':' -f 3))
model_out=($(echo "Test123456" | sed -E "s/[0-9]{5}/Passed/"))
if [ "$out" != "$model_out" ]; then
    echo "Digit test failed!"
    exit 1
fi

out=($(echo "Test123456" | ./$1 "[a-z][0-9]" "Passed" | cut -d ':' -f 3))
model_out=($(echo "Test123456" | sed -E "s/[a-z][0-9]/Passed/"))
if [ "$out" != "$model_out" ]; then
    echo "Mixed test failed!"
    exit 1
fi

out=($(echo "Test123456" | ./$1 "Test" "Passed" | cut -d ':' -f 3))
model_out=($(echo "Test123456" | sed -E "s/Test/Passed/"))
if [ "$out" != "$model_out" ]; then
    echo "Plain text test failed!"
    exit 1
fi

echo "All test have been passed!"
