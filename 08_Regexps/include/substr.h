/**
 * @file
 * @brief Header with esub helper functions
 * @version 1.0
 * @authors AnyKeyShik
 *
 */

#pragma once

/**
 * @brief Generate regex pattern from user input string
 * 
 * @param user_str User string for create pattern from
 *
 * @return Generated pattern for givein string
 */
char *create_pattern(const char *restrict user_str);

/**
 * @brief Compile and execute given regex pattern on given string
 *
 * @param[in] pattern Regex pattern for search
 * @param[in] string String for search in
 * @param[out] start Start index of first occurrence of given pattern on string
 * @param[out] end End index of first occurrence of given pattern on string
 *
 * @return Non-zero value on error and zero on success
 */
int find_pattern(const char *restrict pattern, const char *restrict string,
        int *start, int *end);

/**
 * @brief Replace the substring in string from start to end with given to new substring
 *
 * @param[in,out] string String with substring wich will be replaced
 * @param[in] start Start index of substring for replacement
 * @param[in] end End index of substring for replacement
 * @param[in] replace_str String for replace substring
 *
 * @return Non-zero value on error and zero on success
 */
int substring(char **string, int start, int end, const char *restrict replace_str);
