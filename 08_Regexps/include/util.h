/**
 * @file
 * @brief Service macros and functions for esub
 * @version 1.0
 * @authors AnyKeyShik
 */

#pragma once

#include <stdio.h>

/**
 * @brief Max size of user input
 */
#define BUF_SIZE 4096

/**
 * @brief Max size of error message
 */
#define ERR_SIZE 128

/**
 * @brief Describe possible errors
 */
enum Error
{
    ALLOCATE,   ///< Error in allocate memory for new string
    COPY,       ///< Error in string copy
    CONCAT,     ///< Error in string concat
};

/**
 * @brief Print usage info
 *
 * @param name Program name
 */
void usage(const char *restrict name);

/**
 * @brief Get string from user
 *
 * @param Input stream
 */
char *get_input(FILE *restrict stream);

/**
 * @brief Print detailed info about regex error
 */
void regex_err(int errno);

/**
 * @brief Print detailed error info
 */
void print_err(int errno);
