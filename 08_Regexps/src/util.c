#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>

#include "util.h"

void usage(const char *restrict name)
{
    fprintf(stderr, "Usage: %s <regex> <substitution string>", name);

    exit(1);
}

char *get_input(FILE *restrict stream)
{
    char *user_str = calloc(BUF_SIZE + 1, sizeof(char));
    if (!user_str) {
        print_err(ALLOCATE);
    }

    fputs("Please enter string for substring replacement: ", stdout);
    fgets(user_str, BUF_SIZE, stream);

    user_str[strlen(user_str) - 1] = '\0';

    return user_str;
}

void regex_err(int errno)
{
    char error[ERR_SIZE];

    regerror(errno, NULL, error, ERR_SIZE);
    fputs(error, stderr);

    exit(5);
}

void print_err(int errno)
{
    char *error;

    switch (errno) {
        case ALLOCATE:
            error = "Error in allocate memory for new string";
            break;
        case COPY:
            error = "Error in string copy";
            break;
        case CONCAT:
            error = "Error in string concat";
            break;
        default:
            error = "Unkonwn error";
            break;
    }
    fputs(error, stderr);

    exit(10);
}

