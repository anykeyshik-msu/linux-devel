/**
 * @mainpage esub
 * @code 
 * ./esub <regex> <substitution string>
 * @endcode
 *
 * @file
 * @brief Main file for this project
 * @version 1.0
 * @authors AnyKeyShik
 */

#include <stdio.h>
#include <stdlib.h>

#include "util.h"
#include "substr.h"

int main(int argc, char *argv[])
{
    if (argc < 3) {
        usage(argv[0]);
    }

    char *pattern = create_pattern(argv[1]);
    char *replace_str = argv[2];
    char *user_str = get_input(stdin);

    int substr_strat;
    int substr_end;
    int errno;
    
    errno = find_pattern(pattern, user_str, &substr_strat, &substr_end);
    if (errno) {
        regex_err(errno);
    }

    errno = substring(&user_str, substr_strat, substr_end, replace_str);
    if (errno) {
        print_err(errno);
    }

    printf("Result string: %s\n", user_str);
    
    free(pattern);
    free(user_str);

    return 0;
}
