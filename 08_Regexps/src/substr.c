#include <regex.h>
#include <stdlib.h>
#include <string.h>

#include "substr.h"
#include "util.h"

#include <stdio.h>

char *create_pattern(const char *restrict user_str)
{
    size_t input_len = strlen(user_str);
    char *pattern = calloc(input_len + 3, sizeof(char));

    pattern[0] = '(';
    strncat(pattern, user_str, input_len);
    pattern[input_len + 1] = ')';

    return pattern;
}

int find_pattern(const char *restrict pattern, const char *restrict string, 
        int *start, int *end)
{
    regex_t regex;
    regmatch_t match;
    int retval;

    retval = regcomp(&regex, pattern, REG_EXTENDED);
    if (retval) {
        return retval;
    }

    retval = regexec(&regex, string, 1, &match, 0);
    *start = match.rm_so;
    *end = match.rm_eo;

    return retval;
}

int substring(char **string, int start, int end, 
        const char *restrict replace_str)
{
    size_t str_len = strlen(*string);
    size_t replace_len = strlen(replace_str);
    size_t substr_len = end - start;
    size_t new_len = str_len - substr_len + replace_len + 1;

    char *new_string = calloc(new_len, sizeof(char));
    if (!new_string) {
        return ALLOCATE;
    }

    strncpy(new_string, *string, start);
    if (!new_string) {
        return COPY;
    }

    strncat(new_string, replace_str, replace_len);
    if (!new_string) {
        return CONCAT;
    }

    strncat(new_string, *string + end, str_len - end);
    if (!new_string) {
        return CONCAT;
    }

    free(*string);
    *string = new_string;

    return 0;
}
