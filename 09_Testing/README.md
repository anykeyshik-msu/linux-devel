Тестирование
============

Текст задания
-------------
0. Изучить выбранный вами фреймворк тестов для Си
    * Например, `check`, `autotools` и [пример из лекции](https://git.sr.ht/~frbrgeorge/namegen)
        * В ALT Linux понадобятся:
            * `make`, `automake`, `autoconf`
            * `libtool`
            * `libcheck-devel`, `check`
            * (кажется, всё?) 
1. Взять за основу [Growable Memory Buffers for C99](https://github.com/skeeto/growable-buf)
    * Превратить в библиотеку
        * Функция в ней всего *одна*, остальное макросы, и переделывать это не надо
        * Все макросы уезжают в `.h`-файл
        * *Приложение*-пример можно не писать
        * Тесты взять из авторского файла `tests.c`
            * Тесты на память с `setjmp()` *можно* выкинуть.
        * Оформить их сообразно правилам фреймворка в несколько отдельных тестов
            * (необязательно) попробовать добиться как можно более полного покрытия (предельные тексты, например, с помощью [prlimit](http://man7.org/linux/man-pages/man2/prlimit.2.html))
        * Сделать примитивную поддержку проверки покрытия (чтобы проценты показывало)
2. В репоизтории с домашними заданиями создать подкаталог `09_Testing` и положить решение туда

Итоги
-----
* Сборка и тестирование с помощью `CMake` и `Meson`
* **CMake** 
    1. Конфигурация: `mkdir <build dir> && cd <build dir>`
    2. Сборка: `cmake .. && make`
    3. Тестирование: `make test`
    4. Удаление конфигурации: `cd .. && rm -rf <build dir>`
* **Meson**
    1. Конфигурация:
        * Создание новой конфигурации: `meson setup <build dir>`
        * Обновеление существующей конфигурации: `meson setup --wipe <build dir>`
    *Все шаги далее осуществляются из директории, созданной на прошлом этапе*
    2. Сборка: `meson compile`
    3. Тестирование `meson test`
    4. Удаление конфигурации: `meson compile all_clean; cd ..`
