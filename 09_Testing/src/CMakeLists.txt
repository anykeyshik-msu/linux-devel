project(buf)

set(buf_source buf.c)

add_library(buf STATIC ${buf_source})
