#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>

#ifdef READLINE
#include <readline/readline.h>
#endif

#include "hash.h"

size_t BUF_SIZE = 4096;

int main(void)
{
    char *hash;
    char *input;
    hash = NULL;

#ifndef READLINE
    input = calloc(BUF_SIZE + 1, sizeof(char));
    
    fputs("> ", stdout);
    while ( getline(&input, &BUF_SIZE, stdin) != -1 ) {
#else
    input = readline("> ");
    while (input) {
#endif

        if ( !process_input(input, &hash) ) {
            puts(hash);
            free(hash);
        }

#ifndef READLINE 
        fputs("> ", stdout);
#else
        input = readline("> ");
#endif
    }

    return 0;
}
