#include <rhash.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>

#include "hash.h"

/**
 * Compute a hash of given message/file
 *
 * @param hash_id id of message digest to compute
 * @param input the user input to process (message or filename)
 * @param is_file is user input is filename
 * @param digest buffer to receive the binary message digest value
 * @return 0 on success, -1 on error
 */
static int hash_input(unsigned int hash_id, const char *input, bool is_file, unsigned char *digest)
{
    int res = -1;

    if (is_file) {
        res = rhash_file(hash_id, input, digest);
    } else {
        res = rhash_msg(hash_id, input, strlen(input), digest);
    }

    return res;
}

int process_input(const char *input, char **hash)
{
    unsigned char digest[64];

    unsigned int hash_id;
    unsigned int flags;
    bool is_file;

    /* Prepare and calc binary message digest */
    char *process = strdup(input);
    char *hash_type = strtok(process, " ");
    char *message = strtok(NULL, " ");

    if (message) {
        message[strcspn(message, "\n")] = 0;
        is_file = false;

        if (message[0] != '"') {
            is_file = true;
        } else {
            message += 1;
        }
    } else {
        errno = ENOTSUP;
        perror("No hash provided!");

        free(process);
        
        return -1;
    }
    
    if ( !strcasecmp(hash_type, "MD5") ) {
        hash_id = RHASH_MD5;
    }
    else if ( !strcasecmp(hash_type, "SHA1") ) {
        hash_id = RHASH_SHA1;
    }
    else if ( !strcasecmp(hash_type, "TTH") ) {
        hash_id = RHASH_TIGER;
    } else {
        errno = ENOTSUP;
        perror("Invalid hash type!");

        free(process);
        
        return -1;
    }

    if ( hash_input(hash_id, message, is_file, digest) ) {
        errno = EIO;
        perror("Error while calc hash!");

        free(process);
        
        return -1;
    }

    /* Convert binary message digest into human-readable form and return it */
    if (!*hash) {
        *hash = calloc(130, sizeof(char));
    }

    if ( isupper(hash_type[0]) ) {
        flags = RHPR_HEX;
    } else {
        flags = RHPR_BASE64;
    }

    rhash_print_bytes(*hash, digest, rhash_get_digest_size(hash_id), flags);

    *hash = realloc(*hash, sizeof(*hash) * strlen(*hash));
    free(process);

    return 0;
}
