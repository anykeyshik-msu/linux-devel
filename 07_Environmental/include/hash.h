#ifndef HASH_H
#define HASH_H

#include <rhash.h>
#include <stdbool.h>

/**
 * Process user input and calc the hash
 *
 * @param input user input
 * @param hash buffer to receive the string hash. If not initialized it will be initialized
 * @return 0 in success or non-zero value in error case
 */
int process_input(const char *input, char **hash);

#endif
