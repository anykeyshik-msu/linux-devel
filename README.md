# Linux Development practicum on CMC MSU 2021


### About
[Course info](https://uneex.org/LecturesCMC/LinuxApplicationDevelopment2021)

[Lector's repo](https://github.com/FrBrGeorge/LinuxDevelopment2021)


### Homeworks for "Linux Application Development" course in CMC MSU
* [Сборочное окружение](./01_BuildReq)
* [Работа с терминалом и простой проект](./02_TerminalProject)
* [Многофайловая сборка](./03_Multifile)
* [Обработка текстов и сценарии](./04_Text)
* [Отладка](./05_Debugging)
* [Трассировка и защита памяти](./06_TracingFencing)
* [Сборочные зависимости и адаптация к окружению](./07_Environmental)
* [Регулярные выражения](./08_Regexps)
* [Тестирование](./09_Testing)
* [Интернационализация и локализация](./10_I18n)
* [Информационное пространство: документирование](./11_Documenting)
