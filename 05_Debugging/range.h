#ifndef RANGE_H
#define RANGE_H

// ----------------------------- Include section ------------------------------

#include <stdbool.h>

// ------------------------------- Type section -------------------------------

typedef struct 
{
    int start;
    int stop;
    int step;
    
    int current;
} range_t;

// --------------------------- Declaration section ---------------------------- 

void range_init(range_t *range);

bool range_run(range_t *range);

void range_next(range_t *range);

int range_get(range_t *range);

#endif
