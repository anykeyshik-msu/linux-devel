#include <stdlib.h>
#include "range.h"

void range_init(range_t *range)
{
    range->current = range->start;
}

bool range_run(range_t *range)
{
    return range->current < range->stop;
}

void range_next(range_t *range)
{
    range->current += range->step;
}

int range_get(range_t *range)
{
    return range->current;
}

