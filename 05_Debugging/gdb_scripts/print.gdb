command
    silent
    set logging of

    printf "start=%d stop=%d step=%d current=%d\n", range->start, range->stop, range->step, range->current

    set logging on
    continue
end
