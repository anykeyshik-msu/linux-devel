#include <stdio.h>
#include "range.h"

void argparse(int argc, char *argv[], range_t *range);

void usage(const char *name);

int main(int argc, char *argv[])
{
    range_t range;

    argparse(argc, argv, &range);

    for (range_init(&range); range_run(&range); range_next(&range)) {
        printf("%d\n", range_get(&range));
    }

    return 0;
}

void argparse(int argc, char *argv[], range_t *range)
{
    int N, M, S;

    switch (argc)
    {
        case 2:
            N = atoi(argv[1]);

            range->start = 0;
            range->step = 1;
            range->stop = N;

            break;
        case 3:
            M = atoi(argv[1]);
            N = atoi(argv[2]);

            range->start = M;
            range->step = 1;
            range->stop = N;

            break;
        case 4:
            M = atoi(argv[1]);
            N = atoi(argv[2]);
            S = atoi(argv[3]);

            range->start = M;
            range->step = S;
            range->stop = N;

            break;
        default:
            usage(argv[0]);
            break;
    }
}

void usage(const char *name)
{
    printf("Usage: %s <max value> [start value] [step]\n", name);
}
