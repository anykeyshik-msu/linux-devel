/// @mainpage

# NAME
		`number-game` - manual page for number-game v1.0

# SYNOPSIS
       	`number-game [OPTION]`

# DESCRIPTION
       `-r`, `--roman`
              Print numbers in roman format

       `-h, --help`
              Print help

       `-v, --version`
              Print version

       Guesses a number from [1, 3999] with simple questions.

# AUTHOR
       Written by AnyKeyShik Rarity.

# SEE ALSO
       The full documentation for `number-game` is maintained as a Texinfo manual.  If the `info` and `number-game` programs are properly installed at your site, the command

              info number-game

       should give you access to the complete manual.

number-game v1.0
