#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <libintl.h>

#include "guess.h"

int check_input()
{
    char input[DEFAULT_SIZE + 1];

    while(scanf("%s", input) != 0) {
        if ( !strcasecmp(input, _("yes")) ) {
            return 0;
        } else if ( !strcasecmp(input, _("no")) ) {
            return 1;
        } else {
            puts(_("Please answer 'yes' or 'no'"));
        }
    }

    return -1;
}

void print_number(flag_t flag, int number)
{
    if (flag == ARABIC) {
        printf("%d", number);
    } else {
        char *roman = number_to_roman(number);
        printf("%s", roman);
        free(roman);
    }
}

char *number_to_roman(int num)
{
    size_t size = DEFAULT_SIZE;
    size_t ind = 0;
    char *result = calloc(size, sizeof(*result));

	while(num != 0) {
        if (num >= 1000) {        // 1000 - m
            result[ind++] = 'M';
            num -= 1000;
        } else if (num >= 900) {  // 900 -  cm
           result[ind++] = 'C';
           result[ind++] = 'M';
           num -= 900;
        } else if (num >= 500) {  // 500 - d
            result[ind++] = 'D';
            num -= 500;
        } else if (num >= 400) {  // 400 -  cd
            result[ind++] = 'C';
            result[ind++] = 'D';
            num -= 400;
        } else if (num >= 100) {  // 100 - c
            result[ind++] = 'C';
            num -= 100;               
        } else if (num >= 90) {   // 90 - xc
            result[ind++] = 'X';
            result[ind++] = 'C';
            num -= 90;                                              
        } else if (num >= 50) {   // 50 - l
            result[ind++] = 'L';
            num -= 50;                                                                     
        } else if (num >= 40) {   // 40 - xl
            result[ind++] = 'X';
            result[ind++] = 'L';
            num -= 40;
        } else if (num >= 10) {   // 10 - x
            result[ind++] = 'X';
            num -= 10;           
        } else if (num >= 9) {    // 9 - ix
            result[ind++] = 'I';
            result[ind++] = 'X';
            num -= 9;               
        } else if (num >= 5) {    // 5 - v
            result[ind++] = 'V';
            num -= 5;                                     
        } else if (num >= 4) {    // 4 - iv
            result[ind++] = 'I';
            result[ind++] = 'V';
            num -= 4;                                                   
        } else if (num >= 1) {    // 1 - i
            result[ind++] = 'I';
            num -= 1;                                                                                   
        }

        if (ind + 2 == size) {
            size *= 2;
            result = realloc(result, size * sizeof(*result));
        }
    }

    result = realloc(result, (ind + 1) * sizeof(*result));
    result[ind] = '\0';

    return result;
}

void print_help(char *name)
{
    fputs(_("Usage: "), stdout);
    fputs(name, stdout);
    puts(_(" [OPTION]\n"));
    puts(_("\t-r, --roman\t\tPrint numbers in roman format"));
    puts(_("\t-h, --help\t\tPrint help"));
    puts(_("\t-v, --version\t\tPrint version"));
    puts(_("Guesses a number from [1, 3999] with simple questions."));
}

void print_version(char *name)
{
    printf("%s v%.1lf\n\n", name, VERSION);
    puts(_("Written by AnyKeyShik Rarity."));
}
