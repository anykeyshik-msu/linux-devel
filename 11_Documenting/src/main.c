/**
 * @file
 * @brief Main file for this project
 * @authors AnyKeyShik
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#include <libintl.h>

#include "guess.h"

int main(int argc, char *argv[])
{
    setlocale(LC_ALL, "");
    bindtextdomain("number-game", LOCALE_PATH);
    textdomain("number-game");

    flag_t flag = ARABIC;
    int min = MIN;
    int max = MAX + 1;

    if (argc >= 2) {
        if (!strcmp(argv[1], "-r") || !strcmp(argv[1], "--roman")) {
            flag = ROMAN;
        }

        if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
            print_help(argv[0]);
            exit(0);
        }

        if (!strcmp(argv[1], "-v") || !strcmp(argv[1], "--version")) {
            print_version(argv[0]);
            exit(0);    
        }
    } 

    fputs(_("Please guess a number from "), stdout);
    print_number(flag, MIN);
    fputs(_(" to "), stdout);
    print_number(flag, MAX);
    fputs("\n", stdout);

    while (max - min > 0) {
        int mid = (max + min) / 2;

        fputs(_("Is the number higher than "), stdout);
        print_number(flag, mid);
        puts(_("?[yes/no]"));
        if (!check_input()) {
            min = mid + 1;
        } else {
            max = mid;
        }
    }

    fputs(_("Your number is "), stdout);
    print_number(flag, min);
    puts("!");

    return 0;
}
