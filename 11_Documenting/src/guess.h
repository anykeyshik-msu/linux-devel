/**
 * @file
 * @brief Header with constants, macros for localisation and function declaration
 * @authors AnyKeyShik
 */

#pragma once

#define _(STRING) gettext(STRING)
#define LOCALE_PATH "."
#define VERSION 1.0

/**
 * @brief Constant define
 */
enum
{
    DEFAULT_SIZE = 5,
    MIN = 1,
    MAX = 3999,
    ARABIC = 0,
    ROMAN = 1
};

/**
 * @brief Flag for roman numbers
 */
typedef unsigned char flag_t;

/**
 * @brief Get user input and check for "yes" or "no" answer
 *
 * @param void
 * @return 0 in "yes" case and 1 for otherwise
 */
int check_input(void);

/**
 * @brief Print number in user-specfied format (arabic or roman)
 *
 * @param flag Set the format of number. Can be ARABIC or ROMAN
 * @param number Number for print
 */
void print_number(flag_t flag, int number);

/**
 * @brief Convert number to string for print roman
 *
 * @param number Number for convert
 *
 * @return Roman string
 */
char * number_to_roman(int number);

/**
 * @brief Print help info for this program
 *
 * @param name Name of the program
 */
void print_help(char *name);

/**
 * @brief Print help info for this program
 *
 * @param name Name of the program
 */
void print_version(char *name);
