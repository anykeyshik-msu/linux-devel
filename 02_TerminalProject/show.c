#include <locale.h>
#include <ncurses.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sysexits.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>


// ------------------------------- Type section -------------------------------
typedef void callback_t(void);
typedef struct {
    int key;
    callback_t *callback;
} key_bind_t;

typedef struct {
    int width;
    int height;
    int x;
    int y;
} win_size_t;

typedef struct {
    char *data;
    int width;
    int height;
    int x;
    int y;
} text_t;


// ------------------- Global variables for callback functions ----------------
WINDOW *main_win;
win_size_t main_win_size;

WINDOW *text_win;
win_size_t text_win_size;

WINDOW *text_pad;
win_size_t text_pad_size;

text_t content;

bool in_progress;

char *filename;


// --------------------------- Callbacks functions ----------------------------
void invalid_key(void);
void quit(void);

void mv_up(void);
void mv_down(void);
void mv_left(void);
void mv_right(void);
void mv_next_page(void);
void mv_prev_page(void);


// ------------- Funcs for read and print out content from file ---------------
void readfile();

void print_content(WINDOW *win, const char *msg);


// ------------------------- Funcs for window control -------------------------
void recalc_size(void);
void resize(void);
void update(int dx, int dy);


// ------------------------------ Main function -------------------------------
int main(int argc, char *argv[])
{
    // Args parse
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <filename>\n", argv[0]);
        exit(EX_USAGE);
    }
    filename = argv[1];
    readfile();

    // Init ncurses and base setup
    in_progress = true;
    main_win = initscr();
    if (!main_win) {
        perror("Error while initializing ncurses!");
        exit(EX_SOFTWARE);
    }
    setlocale(LC_ALL, "en_US.UTF-8");
    noecho();
    cbreak();
    set_escdelay(0);
    curs_set(0);
    keypad(main_win, true);
    recalc_size();

    // Create curses window for text
    text_win = subwin(main_win, text_win_size.height, text_win_size.width, text_win_size.x, text_win_size.y);
    box(text_win, 0, 0);

    // Create curses pad for text and print out text to it
    text_pad = newpad(content.height, content.width + 1);
    print_content(text_pad, content.data);
    resize();

    // Map callbacks to keys
    callback_t *default_callback = invalid_key;
    key_bind_t key_binds[] = {
        // ESC/q - quit
        {.key = 'q', .callback =  quit},
        {.key = '\e', .callback = quit},

        // Space/Arrow Down/Enter - next line
        {.key = ' ', .callback = mv_down},
        {.key = '\n', .callback = mv_down},
        {.key = KEY_ENTER, .callback = mv_down},
        {.key = KEY_DOWN, .callback = mv_down},

        // Up/Left/Right
        {.key = KEY_UP, .callback = mv_up},
        {.key = KEY_RIGHT, .callback = mv_right},
        {.key = KEY_LEFT, .callback = mv_left},

        // PageUp/PageDown - next/previous page
        {.key = KEY_PPAGE, .callback = mv_next_page},
        {.key = KEY_NPAGE, .callback = mv_prev_page},
    };
    size_t key_binds_amount = sizeof(key_binds) / sizeof(key_binds[0]);

    // Work cycle
    while (in_progress) {
        int user_key = wgetch(main_win);
        
        callback_t *callback = default_callback;
        for (size_t i = 0; i < key_binds_amount; i++) {
            key_bind_t current_kb = key_binds[i];

            if (current_kb.key == user_key) {
                callback = current_kb.callback;
                break;
            }
        }

        callback();
    }
    
    // Free and exit
    free(content.data);
    delwin(text_pad);
    delwin(text_win);
    endwin();

    return 0;
}


// --------------------------- Declaration section ----------------------------

/**
 * Read content from flie
 */
void readfile()
{
    struct stat buf;

    // Read file
    FILE *file = fopen(filename, "r");
    if (!file) {
        perror("Error opening file!");
        exit(EX_NOINPUT);
    }
    
    fstat(fileno(file), &buf);
    size_t filesize = buf.st_size;

    content.data = calloc(filesize + 1, sizeof(char));
    fread(content.data, 1, filesize, file);
    content.data[filesize] = '\0';

    fclose(file);

    // Calc content sizes (longest line and number of lines)
    content.height = 0;
    content.width = 0;
    
    size_t curr_pos = 0;
    int line_len = 0;
    while (curr_pos < filesize) {
        if (curr_pos +1 == filesize || content.data[curr_pos] == '\n') {
            content.height += 1;

            if (content.width < line_len) {
                content.width = line_len;
            }

            line_len = 0;
        } else {
            line_len += 1;
        }

        curr_pos += 1;
    }

    content.x = 0;
    content.y = 0;
}

/**
 * This function is a workaround for a stupid bug in ncurses that doesn't
 * allow you to print more than LINES * (COLS+1) characters at once with
 * wprintw due to an undersized internal buffer.
 */
void print_content(WINDOW *win, const char *msg)
{
    size_t bufsize = LINES * (COLS + 1) - 1;
    size_t remaining_len = strlen(msg);

    while (remaining_len > bufsize) {
        wprintw(win, "%.*s", bufsize, msg);
        msg += bufsize;
        remaining_len -= bufsize;
    }
    wprintw(win, "%s\n", msg);
}

/**
 * Recalc text window and pad sizes
 */
void recalc_size(void)
{
    text_win_size.height = LINES - 1;
    text_win_size.width = COLS;
    text_win_size.x = 1;
    text_win_size.y = 0;

    text_pad_size.height = LINES - 3;
    text_pad_size.width = COLS - 2;
    text_pad_size.x = 2;
    text_pad_size.y = 1;
}

/**
 * Resize windows
 */
void resize(void)
{
    recalc_size();
    update(0, 0);
    wrefresh(text_win);
    wrefresh(main_win);
    update(0, 0);
}

/**
 * Calc text coordinates
 */
int clip(int x, int lo, int hi)
{
    if (x >= hi) {
        x = hi;
    }

    if (x <= lo) {
        x = lo;
    }

    return x;
}

/**
 * Update window for new content
 */
void update(int dx, int dy)
{
    content.x = clip(content.x + dx, 0, content.height - text_pad_size.height);
    content.y = clip(content.y + dy, 0, content.width - text_pad_size.width);

    wmove(main_win, 0, 0);
    wclrtoeol(main_win);
    wprintw(main_win, "file: %s [%d:%d]", filename, content.x, content.y);

    mvwin(text_win, text_win_size.x, text_win_size.y);
    wresize(text_win, text_win_size.height, text_win_size.width);
    box(text_win, 0, 0);

    prefresh(
        text_pad,
        content.x, content.y,
        text_pad_size.x, text_pad_size.y,
        text_pad_size.height + 1, text_pad_size.width
    );
}


// ---------------------------- Callback serction -----------------------------
void invalid_key(void)
{
    return;
}

void quit(void)
{
    in_progress = false;
}

void mv_up(void)
{
    update(-1, 0);
}

void mv_down(void)
{
    update(1, 0);
}

void mv_right(void)
{
    update(0, 1);
}

void mv_left(void)
{
    update(0, -1);
}

void mv_next_page(void)
{
    update(-text_pad_size.height, 0);
}

void mv_prev_page(void)
{
    update(text_pad_size.height, 0);
}
