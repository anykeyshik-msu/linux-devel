#pragma once

#define _(STRING) gettext(STRING)
#define LOCALE_PATH "."

#define INPUT_SIZE 5

int check_input(void);
