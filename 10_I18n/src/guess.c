#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <locale.h>
#include <libintl.h>

#include "guess.h"

int check_input()
{
    char input[INPUT_SIZE + 1];

    while(scanf("%5s", input) != 0) {
        if ( !strcasecmp(input, _("yes")) ) {
            return 0;
        } else if ( !strcasecmp(input, _("no")) ) {
            return 1;
        } else {
            puts(_("Please answer 'yes' or 'no'"));
        }
    }

    return -1;
}
