#include <stdio.h>
#include <locale.h>
#include <libintl.h>

#include "guess.h"

int main(void)
{
    setlocale(LC_ALL, "");
    bindtextdomain("localisation", LOCALE_PATH);
    textdomain("localisation");

    int min = 1;
    int max = 101;

    puts(_("Please guess a number from 1 to 100"));

    while (max - min > 0) {
        int mid = (max + min) / 2;

        printf(_("Is the number higher than %d? [yes/no]\n"), mid);
        if (!check_input()) {
            min = mid + 1;
        } else {
            max = mid;
        }
    }

    printf(_("Your number is %d!\n"), min);

    return 0;
}
