#!/bin/sh

if [ "${#}" -gt 1  ] || [ -t 0 ]; then
    printf "Usage:\n\tcat [input] | ${0} [delay]\n" >&2; exit -1
fi
DELAY="${1:-0}"

TEXT=$(cat - | hexdump -ve '/1 "%o\n"')

LINE_NO=0
CHAR_NO=0
INDEXED_TEXT=""
while read CHAR; do
    if [ "${CHAR}" -ne 40 ]; then
        INDEXED_TEXT=$(printf "%s %s %s\n%s" "${LINE_NO}" "${CHAR_NO}" "${CHAR}" "${INDEXED_TEXT}")
    fi
    CHAR_NO=$((CHAR_NO + 1))
    if [ "${CHAR}" -eq 12 ]; then
        LINE_NO=$((LINE_NO + 1)); CHAR_NO=0
    fi
done <<EOF
${TEXT}
EOF

put() {
    tput cup "${1}" "${2}"
    printf '\'"${3}"
}


tput clear
shuf <<EOF |
${INDEXED_TEXT}
EOF
    while read CHAR2; do
        put ${CHAR2}; sleep ${DELAY}
    done

tput cup "${LINE_NO}" "0"
