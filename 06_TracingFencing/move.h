#ifndef MOVE_H
#define MOVE_H

#define BUF_SIZE 4096

void check_state(const char *error);
void move(const char *src_filename, const char *dst_filename);

#endif
