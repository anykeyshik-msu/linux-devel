#include <sysexits.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include "move.h"

void check_state(const char *error)
{
    int status = 0;

    switch (errno) 
    {
        case 0:
            break;
        case EINVAL:
        case EISDIR:
        case ELOOP:
        case ENAMETOOLONG:
            status = EX_USAGE;
            break;
        case EACCES:
            status = EX_NOPERM;
            break;
        case ENOENT:
            status = EX_NOINPUT;
            break;
        default:
            status = EX_SOFTWARE;
            break;
    }

    if (status) {
        perror(error);
        exit(status);
    }
}

void move(const char *src_filename, const char *dst_filename)
{
    FILE *src_file = fopen(src_filename, "r");
    check_state("Error while open the source file!");

    FILE *dst_file = fopen(dst_filename, "w");
    check_state("Error while open the destination file!");

    size_t readed = 0;
    char buf[BUF_SIZE];
    while (( readed = fread(buf, sizeof(char), BUF_SIZE, src_file) )) {
        check_state("Error while read data from source file!");

        fwrite(buf, sizeof(char), readed, dst_file);
        check_state("Error while write data to destination file!");
    }

    fclose(dst_file);
    check_state("Error while close destination file!");

    fclose(src_file);
    check_state("Error while close source file!");

    remove(src_filename);
    check_state("Error while delete source file!");
}
