#define _GNU_SOURCE
#include <dlfcn.h>
#include <string.h>
#include <stdio.h>

typedef int func_ptr(const char *);
func_ptr remove;

int remove(const char *pathname)
{
    if (strstr(pathname, "PROTECT")) {
        perror("Can't remove source file due protection!");

        return 0;
    }

    func_ptr *my_remove = dlsym(RTLD_NEXT, "remove");
    return my_remove(pathname);
}
