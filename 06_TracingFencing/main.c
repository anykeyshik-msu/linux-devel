#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include "move.h"

void usage(const char *name);

int main(int argc, char *argv[])
{
    if (argc != 3) {
        usage(argv[0]);
        exit(EX_USAGE);
    }

    move(argv[1], argv[2]);

    return 0;
}

void usage(const char *name)
{
    printf("Usage: %s <source file> <destination file>\n", name);
}
