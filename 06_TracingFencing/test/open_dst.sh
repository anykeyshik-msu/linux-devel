#!/bin/sh

TEMP=$(./test/generator.sh destination openat EIO)

cmp "${TEMP}/source" "${TEMP}/model_source" \
    && test ! -e "${TEMP}/destination"
