#!/bin/sh

TEMP=$(mktemp -d)

base64 /dev/random | head > "${TEMP}/source"
cp "${TEMP}/source" "${TEMP}/model_source"

./move \
    "${TEMP}/source" \
    "${TEMP}/destination" \
    && cmp "${TEMP}/model_source" "${TEMP}/destination" \
    && test ! -e "${TEMP}/source"
