#!/bin/sh

TEMP=$(./test/generator.sh destination close EIO)

cmp "${TEMP}/source" "${TEMP}/model_source" \
    && cmp "${TEMP}/source" "${TEMP}/destination"
