#!/bin/sh

TEMP=$(./test/generator.sh source unlink EIO)
cmp "${TEMP}/source" "${TEMP}/model_source" \
    && cmp "${TEMP}/source" "${TEMP}/destination"
