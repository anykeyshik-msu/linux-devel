#!/bin/sh

TEMP=$(mktemp -d)

base64 /dev/random | head > "${TEMP}/source"
cp "${TEMP}/source" "${TEMP}/model_source"

strace \
    -P "${TEMP}/${1}" \
    -e fault=${2}:error=${3} \
    ./move \
    "${TEMP}/source" \
    "${TEMP}/destination" \
    >/dev/null 2>/dev/null

echo "${TEMP}"
