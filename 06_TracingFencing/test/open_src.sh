#!/bin/sh

TEMP=$(./test/generator.sh source openat EIO)

cmp "${TEMP}/source" "${TEMP}/model_source" \
    && test ! -e "${TEMP}/destination"
