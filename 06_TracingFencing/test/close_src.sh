#!/bin/sh

TEMP=$(./test/generator.sh source close EIO)

cmp "${TEMP}/source" "${TEMP}/model_source" \
    && cmp "${TEMP}/source" "${TEMP}/destination"
